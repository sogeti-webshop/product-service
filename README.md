# ProductService

This library contains everything that is necessary to get information about products.

## Sonar

To verify testcoverage, you can publish to sonar by running the `publish-to-sonar.sh` script.

## CI/CD

To have this package available in other parts of the application, this application needs to be transformed into a JAR file and stored at a repository. Since I currently have no access to a online repository, the local .ivy2 repository will be used. Run the `build-and-publish.sh` script to test, compile and build the artifact. This will then be stored locally and other services can be build.
