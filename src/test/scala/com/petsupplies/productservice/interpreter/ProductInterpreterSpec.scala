package com.petsupplies.productservice.interpreter

import com.petsupplies.productservice.interpreter.ProductInterpreterSpec.productRepositoryStub
import cats.~>
import com.petsupplies.productservice.dsl.ProductAction
import com.petsupplies.productservice.dsl.ProductAction.{AllProducts, GetProduct}
import com.petsupplies.productservice.persistence.ProductRepository
import com.petsupplies.productservice.types.Product.{Product, ProductId}
import fs2.{Strategy, Task}
import org.scalatest.{FlatSpec, Matchers}

import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global
import cats.instances.future._

class ProductInterpreterSpec extends FlatSpec with Matchers {
  import ProductInterpreterSpec._

  def fixture =
    new {
      val interpreter: ProductAction ~> Future = ProductInterpreter.interpret[Future]
    }

  "ProductInterpreter" should "get all products" in {
    val products = Await.result(
      fixture.interpreter(
        AllProducts()
      ),
      1.seconds
    )

    assert(
      products == List(
        Product(1, "Product1", "Description", 29.95),
        Product(2, "Product2", "Description", 39.95)
      )
    )
  }

  "ProductInterpreter" should "get one product if present" in {
    val product = Await.result(
      fixture.interpreter(
        GetProduct(1)
      ),
      1.seconds
    )

    assert(
      product.get == Product(1, "Product1", "Description", 29.95)
    )
  }

  "ProductInterpreter" should "return Nothing when product is not present" in {
    val product = Await.result(
      fixture.interpreter(
        GetProduct(3)
      ),
      1.seconds
    )

    assert(product.isEmpty)
  }
}

object ProductInterpreterSpec {
  implicit val productRepositoryStub: ProductRepository = new ProductRepository {
    implicit val strategy: Strategy = Strategy.fromExecutionContext(global)

    def getProduct(productId: ProductId): Task[Option[Product]] = Task.now {
      List(
        Product(1, "Product1", "Description", 29.95),
        Product(2, "Product2", "Description", 39.95)
      ).find(_.id == productId)
    }

    def allProducts: Task[List[Product]] = Task.now {
      List(
        Product(1, "Product1", "Description", 29.95),
        Product(2, "Product2", "Description", 39.95)
      )
    }
  }
}