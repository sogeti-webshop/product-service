package com.petsupplies.productservice.types

import org.scalatest.{FlatSpec, Matchers}

class ProductSpec extends FlatSpec with Matchers {
  "Products" should "be instantiatable" in {
    val product = Product.Product.apply(1, "name", "description", 29.95)

    assert(product.id === 1)
    assert(product.name === "name")
    assert(product.description === "description")
    assert(product.price === 29.95)
  }
}
