package com.petsupplies.productservice.implicits

import org.scalacheck.Properties
import org.scalacheck.Prop.forAll
import com.petsupplies.productservice.interpreter.PureProductInterpreter

object ProductFInstancesProp extends Properties("ProductFInstances") {
  property("ProductF applicative Identity law") = forAll {
    value: String => {
        val productF = ProductFInstances.applicative.pure(value)

        ProductFInstances.applicative.ap(
          ProductFInstances.applicative.pure[Function1[String, String]](x => x)
        )(productF).foldMap(PureProductInterpreter.pureInterpret) == value
     }
  }
  // 
  // property("ProductF applicative Interchange law") = forAll {
  //   value: String => {
  //     val app = ProductFInstances.applicative
  //
  //     val y = app.pure(value)
  //     val u = app.pure { s: String => s + "u" }
  //   }
  // }

  property("ProductF applicative Composition law") = forAll {
    value: String => {
      val app = ProductFInstances.applicative

      val w = app.pure(value)
      val v = app.pure { s: String => s + "v" }
      val u = app.pure { s: String => s + "u" }

      app.ap(u)(app.ap(v)(w)).foldMap(PureProductInterpreter.pureInterpret) ==
        app.ap(app.ap(app.ap(app.pure((f: String => String) => (g: String => String) => f compose g))(u))(v))(w).foldMap(PureProductInterpreter.pureInterpret)
    }
  }

  property("ProductF applicative Homomorphism law") = forAll {
    value: String => {
      val app = ProductFInstances.applicative
      val f: String => Int = s => s.length
      app.ap(app.pure(f))(app.pure(value))
        .foldMap(PureProductInterpreter.pureInterpret) == app.pure(f(value)).foldMap(PureProductInterpreter.pureInterpret)
    }
  }
}
