package com.petsupplies.productservice

import com.petsupplies.productservice.interpreter.PureProductInterpreter
import com.petsupplies.productservice.types.Product.Product
import org.scalatest.{FlatSpec, Matchers}

class ProductServiceSpec extends FlatSpec with Matchers {
  "A ProductService" should "give all products" in {
    val actualProducts = ProductService.all.foldMap(PureProductInterpreter.pureInterpret)
    val expectedProducts = PureProductInterpreter.products

    assert(actualProducts === expectedProducts)
  }

  "A ProductService" should "give a specific product" in {
    val actualProduct1 = ProductService.get(1).foldMap(PureProductInterpreter.pureInterpret)
    val expectedProduct1 = Product(1, "Product1", "Description", 29.95)

    assert(actualProduct1.get === expectedProduct1)

    val actualProduct2 = ProductService.get(2).foldMap(PureProductInterpreter.pureInterpret)
    val expectedProduct2 = Product(2, "Product2", "Description", 39.95)

    assert(actualProduct2.get === expectedProduct2)
  }

  "A ProductService" should "return Nothing when product not found" in {
    val actualProduct = ProductService.get(3).foldMap(PureProductInterpreter.pureInterpret)

    assert(actualProduct.isEmpty)
  }

  "A ProductService" should "return all products if all ids are present" in {
    val result = ProductService.getProducts(List(1, 2)).foldMap(PureProductInterpreter.pureInterpret)

    assert(
      List(
        Some(Product(1, "Product1", "Description", 29.95)),
        Some(Product(2, "Product2", "Description", 39.95))
      ) == result
    )
  }

  "A ProductService" should "return Nothing for missing products" in {
    val result = ProductService.getProducts(List(1, 2, 3)).foldMap(PureProductInterpreter.pureInterpret)

    assert(
      List(
        Some(Product(1, "Product1", "Description", 29.95)),
        Some(Product(2, "Product2", "Description", 39.95)),
        None
      ) == result
    )
  }
}
