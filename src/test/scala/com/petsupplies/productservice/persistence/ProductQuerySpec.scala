package com.petsupplies.productservice.persistence

import doobie.scalatest.QueryChecker
import doobie.util.iolite.IOLite
import doobie.util.transactor.Transactor
import org.scalatest.{FunSuite, Matchers}
import com.petsupplies.productservice.persistence.ProductQuery.productQuery

class ProductQuerySpec extends FunSuite with Matchers with QueryChecker {
  def transactor: Transactor[IOLite] = TestTransactor.transactor

  test("allProducts") { checkOutput(productQuery.all) }
  test("getProduct") { checkOutput(productQuery.get(0)) }
}

