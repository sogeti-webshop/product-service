package com.petsupplies.productservice.persistence

import doobie.imports.{DriverManagerTransactor, Transactor}
import doobie.util.iolite.IOLite

object TestTransactor {
  val transactor: Transactor[IOLite] =
    DriverManagerTransactor[IOLite](
      "com.mysql.jdbc.Driver",
      "jdbc:mysql://localhost:3306/petsupplies",
      "root",
      ""
    )
}
