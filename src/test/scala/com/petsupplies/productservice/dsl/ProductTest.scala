package com.petsupplies.productservice.dsl

import com.petsupplies.productservice.interpreter.PureProductInterpreter
import org.scalatest.{FlatSpec, Matchers}

class ProductTest extends FlatSpec with Matchers {
  "allProducts" should "lift AllProducts in Free" in {

    assert(
      Product.allProducts().foldMap(PureProductInterpreter.pureInterpret) == PureProductInterpreter.products
    )
  }

  "getProducts" should "lift GetProducts in Free" in {
    assert(
      Product.getProduct(1).foldMap(PureProductInterpreter.pureInterpret) == PureProductInterpreter.products.find(_.id == 1)
    )
  }
}
