package com.petsupplies.productservice.types

object Product {
  type ProductId = Int
  type Name = String
  type Description = String
  type Price = BigDecimal

  case class Product(id: ProductId, name: Name, description: Description, price: Price)
}

