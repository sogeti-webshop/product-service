package com.petsupplies.productservice.dsl

import cats.free.Free
import com.petsupplies.productservice.dsl.ProductAction.{AllProducts, GetProduct}
import com.petsupplies.productservice.types.Product.{ProductId, Product => DomainProduct}

object Product {
  type ProductF[A] = Free[ProductAction, A]

  def allProducts(): ProductF[List[DomainProduct]] =
    Free.liftF[ProductAction, List[DomainProduct]](AllProducts())

  def getProduct(productId: ProductId): ProductF[Option[DomainProduct]] =
    Free.liftF[ProductAction, Option[DomainProduct]](GetProduct(productId))
}
