package com.petsupplies.productservice.dsl

import com.petsupplies.productservice.types.Product.{ProductId, Product => DomainProduct}

sealed trait ProductAction[A]

object ProductAction {
  final case class AllProducts() extends ProductAction[List[DomainProduct]]
  final case class GetProduct(id: ProductId) extends ProductAction[Option[DomainProduct]]
}
