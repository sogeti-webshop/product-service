package com.petsupplies.productservice

import cats.Applicative
import com.petsupplies.productservice.dsl.Product.{allProducts, getProduct}
import com.petsupplies.productservice.dsl.Product.ProductF
import com.petsupplies.productservice.types.Product.{Product, ProductId}
import cats.instances.list._
import com.petsupplies.productservice.implicits.ProductFInstances._

object ProductService {
  def all: ProductF[List[Product]] = allProducts()
  def get(productId: ProductId): ProductF[Option[Product]] = getProduct(productId)
  def getProducts(productIds: List[ProductId]): ProductF[List[Option[Product]]] =
    Applicative[ProductF].sequence(productIds.map(get))
}
