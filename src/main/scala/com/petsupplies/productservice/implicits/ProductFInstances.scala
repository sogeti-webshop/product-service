package com.petsupplies.productservice.implicits

import cats.Applicative
import cats.free.Free
import com.petsupplies.productservice.dsl.Product.ProductF

object ProductFInstances {
  implicit val applicative: Applicative[ProductF] = new Applicative[ProductF] {
    def pure[A](x: A): ProductF[A] = Free.pure(x)

    def ap[A, B](ff: ProductF[(A) => B])(fa: ProductF[A]): ProductF[B] = ff.flatMap(f => fa.map(f))
  }
}
