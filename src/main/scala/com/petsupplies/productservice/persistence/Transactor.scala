package com.petsupplies.productservice.persistence

import doobie.imports.{DriverManagerTransactor, Transactor}
import fs2.Task

object Transactor {
  implicit val transactor: Transactor[Task] =
    DriverManagerTransactor[Task]("com.mysql.jdbc.Driver", "jdbc:mysql://mysql:3306/petsupplies", "root", "")
}
