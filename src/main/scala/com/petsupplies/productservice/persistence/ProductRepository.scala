package com.petsupplies.productservice.persistence

import doobie.imports._
import fs2.Task
import com.petsupplies.productservice.types.Product.{Product, ProductId}

trait ProductRepository {
  def allProducts: Task[List[Product]]

  def getProduct(productId: ProductId): Task[Option[Product]]
}

object ProductRepository {
  import com.petsupplies.productservice.persistence.ProductQuery._

  implicit def doobieProductRepository(implicit productQuery: ProductQuery, transactor: Transactor[Task]): ProductRepository =
    new ProductRepository {
      def getProduct(productId: ProductId): Task[Option[Product]] =
        productQuery
            .get(productId)
            .option
            .transact(transactor)

      def allProducts: Task[List[Product]] =
        productQuery
            .all
            .list
            .transact(transactor)
    }
}
