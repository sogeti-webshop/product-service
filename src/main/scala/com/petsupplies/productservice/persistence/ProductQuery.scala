package com.petsupplies.productservice.persistence

import doobie.imports._
import com.petsupplies.productservice.types.Product.{Product, ProductId}

trait ProductQuery {
  def all: Query0[Product]

  def get(productId: ProductId): Query0[Product]
}

object ProductQuery {
  implicit val productQuery: ProductQuery = new ProductQuery {
    def all: Query0[Product] =
      sql"select id, name, description, price from products".query[Product]

    def get(productId: ProductId): Query0[Product] =
      sql"select id, name, description, price from products where id = $productId".query[Product]
  }
}
