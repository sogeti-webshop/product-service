package com.petsupplies.productservice.interpreter

import cats.{Id, Monad, ~>}
import com.petsupplies.productservice.dsl.ProductAction.{AllProducts, GetProduct}
import com.petsupplies.productservice.dsl.ProductAction
import com.petsupplies.productservice.persistence.ProductRepository

object ProductInterpreter {
  import com.petsupplies.productservice.persistence.ProductRepository._
  import com.petsupplies.productservice.persistence.ProductQuery._
  import com.petsupplies.productservice.persistence.Transactor.transactor

  def interpret[M[_]](implicit monad: Monad[M], productRepository: ProductRepository): ProductAction ~> M  =
    new (ProductAction ~> M) {
      def apply[A](fa: ProductAction[A]): M[A] =
        fa match {
          case AllProducts() =>
            Monad[M].pure {
                productRepository.allProducts.unsafeRun()
            }

          case GetProduct(productId) =>
            Monad[M].pure {
              productRepository.getProduct(productId).unsafeRun()
            }
        }
    }
}
